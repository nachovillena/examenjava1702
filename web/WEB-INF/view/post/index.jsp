<%-- 
    Document   : index.jsp
    Created on : 09-ene-2017, 19:18:05
    Author     : alumno
--%>

<%@page import="model.Post"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="posts" class="java.util.ArrayList" scope="request"/>  


<!DOCTYPE html>
<%@include file="/WEB-INF/view/header.jsp" %>

<div id="content">
    <h1>Lista de Posts </h1>
    <p>
        <a href="<%= request.getContextPath()%>/post/create">Nuevo post</a>
    </p>
    <div style="padding: 1em;">
        P�ginas: <a href="<%= request.getContextPath()%>/post/paginate/1" style="margin: 0.1em">1</a>
        <a href="<%= request.getContextPath()%>/post/paginate/2" style="margin: 0.1em">2</a>
        <a href="<%= request.getContextPath()%>/post/paginate/3" style="margin: 0.1em">3</a>
        <a href="<%= request.getContextPath()%>/post/paginate/4" style="margin: 0.1em">4</a>
    </div>
    <table>
        <tr>
            <th>Id</th>
            <th>Autor</th>
            <th>Titulo</th>
            <th>Contenido</th>
            <th>Acci�n</th>
        </tr>
        <%        Iterator<model.Post> iterator = posts.iterator();
            while (iterator.hasNext()) {
                Post post = iterator.next();%>
        <tr>
            <td><%= post.getId()%></td>
            <td><%= post.getAuthor()%></td>
            <td><%= post.getTitle()%></td>
            <td><%= post.getContent()%></td>
            <td> 
                <a href="<%= request.getContextPath() + "/post/delete/" + post.getId()%>"> Borrar</a>
                <a href="<%= request.getContextPath() + "/post/edit/" + post.getId()%>"> Editar</a>
                <a href="<%= request.getContextPath() + "/post/view/" + post.getId()%>"> Ver</a>
            </td>
        </tr>
        <%
            }
        %>          </table>
        
        <p>
        <a href="<%= request.getContextPath()%>/post/create">Nuevo post</a>
    </p>
    <div style="padding: 1em;">
        P�ginas: <a href="<%= request.getContextPath()%>/post/paginate/1" style="margin: 0.1em">1</a>
        <a href="<%= request.getContextPath()%>/post/paginate/2" style="margin: 0.1em">2</a>
        <a href="<%= request.getContextPath()%>/post/paginate/3" style="margin: 0.1em">3</a>
        <a href="<%= request.getContextPath()%>/post/paginate/4" style="margin: 0.1em">4</a>
    </div>

</div>
<%@include file="/WEB-INF/view/footer.jsp" %>
