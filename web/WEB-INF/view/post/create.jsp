<%-- 
    Document   : index.jsp
    Created on : 09-ene-2017, 19:18:05
    Author     : alumno
--%>

<%@page import="model.Post"%>
<%@include file="/WEB-INF/view/header.jsp" %>

<div id="content">
    <h1>Alta de Posts </h1>
    <form action="<%= request.getContextPath()%>/post/store" method="post">
        <label>Autor: </label><input name="author" type="text"><br>
        <label>Titulo: </label><input name="title" type="text"><br>
        <label>Contenido: </label><input name="content" type="text"><br>
        <input value="Enviar" type="submit"><br>
    </form>     
</div>
<%@include file="/WEB-INF/view/footer.jsp" %>
