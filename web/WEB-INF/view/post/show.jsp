<%-- 
    Document   : index.jsp
    Created on : 09-ene-2017, 19:18:05
    Author     : alumno
--%>

<%@page import="model.Post"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="post" class="model.Post" scope="request"/>  


<!DOCTYPE html>
<%@include file="/WEB-INF/view/header.jsp" %>

<div id="content">
    <h1> Post: <%= post.getTitle()%> </h1>
    <p><b>Id:</b> <%= post.getId()%></p>
    <p><b>Autor:</b> <%= post.getAuthor()%></p>
    <p><b>Contenido:</b> <%= post.getContent()%></p>
</div>
<%@include file="/WEB-INF/view/footer.jsp" %>
