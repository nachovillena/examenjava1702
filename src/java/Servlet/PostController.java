/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Post;
import persistence.postDAO;

/**
 *
 * @author usuario
 */
public class PostController extends BaseController {

    private static final Logger LOG = Logger.getLogger(PostController.class.getName());
    private postDAO postDAO;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public void index() {     
        //objeto persistencia
        postDAO = new postDAO();
        ArrayList<Post> posts = null;

        //leer datos de la persistencia
        synchronized (postDAO) {
            posts = postDAO.getAll();
        }
        request.setAttribute("posts", posts);
        String name = "index";
        LOG.info("En PostController->" + name);
//        LOG.info("Studies->" + studies.size());
        dispatch("/WEB-INF/view/post/index.jsp");
    }
    public void paginate(String pageString) {
        LOG.info("idString");
        int page = parseInt(pageString);
        //objeto persistencia
        postDAO = new postDAO();
        ArrayList<Post> posts = null;

        //leer datos de la persistencia
        synchronized (postDAO) {
            posts = postDAO.getAllPaginate(page);
        }
        request.setAttribute("posts", posts);
        String name = "index";
        LOG.info("En PostController->" + name);
//        LOG.info("Studies->" + studies.size());
        dispatch("/WEB-INF/view/post/index.jsp");
    }
    public void view(String idString) throws SQLException {
        HttpSession sesion = request.getSession();
        LOG.info("idString");
        long id = toId(idString);
        //objeto persistencia
        postDAO = new postDAO();
        Post post = new Post();

        //leer datos de la persistencia
        synchronized (postDAO) {
            post = postDAO.getPost(id);
        }
        sesion.setAttribute("post", post);
        request.setAttribute("post", post);
        String name = "index";
        LOG.info("En PostController->" + name);
//        LOG.info("Studies->" + studies.size());
        dispatch("/WEB-INF/view/post/show.jsp");
    }
    public void last() throws SQLException {
        HttpSession sesion = (HttpSession) request.getSession();
        Post post = (Post) sesion.getAttribute("post");
        request.setAttribute("post", post);
        dispatch("/WEB-INF/view/post/show.jsp");
    }

    public void create() {
        dispatch("/WEB-INF/view/post/create.jsp");
    }

    public void store() throws IOException {

        //objeto persistencia
        postDAO = new postDAO();
        LOG.info("crear DAO");
        //crear objeto del formulario
        Post post = loadFromRequest();
        
        synchronized (postDAO) {
            try {
                postDAO.insert(post);
            } catch (SQLException ex) {
                LOG.log(Level.SEVERE, null, ex);
                request.setAttribute("ex", ex);
                request.setAttribute("msg", "Error de base de datos ");
                dispatch("/WEB-INF/view/error/index.jsp");
            }
        }
        redirect(contextPath + "/post/index");
    }

    public void edit(String idString) throws SQLException {
        LOG.info("idString");
        long id = toId(idString);
        postDAO  postDAO = new postDAO();
        Post post = postDAO.getPost(id);
        request.setAttribute("module", post);
        dispatch("/WEB-INF/view/module/edit.jsp");

    }

    public void update() throws IOException, SQLException {
        postDAO  postDAO = new postDAO();
        Post post = loadFromRequest();
        postDAO.update(post);
        response.sendRedirect(contextPath + "/post");
        return;
    }

    public void delete(String idString) throws IOException, SQLException 
    {
        LOG.info("BORRANDO " + idString);
        long id = toId(idString);
        
        postDAO  postDAO = new postDAO();
        postDAO.delete(id);
        redirect(contextPath + "/post");

    }
    
    private Post loadFromRequest()
    {
        Post post = new Post();
        LOG.info("Crear modelo");
        post.setId(toId(request.getParameter("id")));
        post.setAuthor(request.getParameter("author"));
        post.setTitle(request.getParameter("title"));
        post.setContent(request.getParameter("content"));
        LOG.info("Datos cargados");
        return post;
    }
}
