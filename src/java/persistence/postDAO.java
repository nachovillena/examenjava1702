/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

//import java.sql.Connection;
//import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Post;

/**
 *
 * @author alumno
 */

public class postDAO extends BaseDAO {

    private static final Logger LOG = Logger.getLogger(postDAO.class.getName());
    private int size_page = 30;
    private int registros;


    public postDAO() {
//        Class.forName("com.mysql.jdbc.Driver");
//        this.connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
    }


    public ArrayList<Post> getAll() {
        PreparedStatement stmt = null;
        ArrayList<Post> posts = null;

        try {
            this.connect();
            stmt = connection.prepareStatement("select * from posts LIMIT 0, "+this.size_page+"");
            ResultSet rs = stmt.executeQuery();
            posts = new ArrayList();

            int i = 0;
            while (rs.next()) {
                i++;
                Post post = new Post();
                post.setId(rs.getLong("id"));
                post.setAuthor(rs.getString("author"));
                post.setTitle(rs.getString("title"));
                post.setContent(rs.getString("content"));

                posts.add(post);
                LOG.info("Registro fila: " + i);
            }
            this.disconnect();
        } catch (SQLException ex) {
            Logger.getLogger(postDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return posts;
    }
    
    public ArrayList<Post> getAllPaginate(int page) {
        PreparedStatement stmt = null;
        ArrayList<Post> posts = null;
        int inicio = (page - 1) * this.size_page;

        try {
            this.connect();
            stmt = connection.prepareStatement("select * from posts LIMIT "+inicio+","+this.size_page);
            ResultSet rs = stmt.executeQuery();
            posts = new ArrayList();

            int i = 0;
            while (rs.next()) {
                i++;
                Post post = new Post();
                post.setId(rs.getLong("id"));
                post.setAuthor(rs.getString("author"));
                post.setTitle(rs.getString("title"));
                post.setContent(rs.getString("content"));

                posts.add(post);
                LOG.info("Registro fila: " + i);
            }
            this.disconnect();
        } catch (SQLException ex) {
            Logger.getLogger(postDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return posts;
    }
    
    public void totalRegistros() throws SQLException{
        PreparedStatement stmt = null;  
        this.connect();
        stmt = connection.prepareStatement("select count(*) as numero from posts");
        ResultSet rs = stmt.executeQuery();
        
        if (rs.next()) {
            LOG.info("Datos ...");
            this.registros = rs.getInt("numero");
    
        }  
    }

    public void insert(Post post) throws SQLException {
        PreparedStatement stmt = null;
        this.connect();
        stmt = connection.prepareStatement(
                "INSERT INTO posts(author, title, content)"
                + " VALUES(?, ?, ?)"
        );
        stmt.setString(1, post.getAuthor());
        stmt.setString(2, post.getTitle());
        stmt.setString(3, post.getContent());
    

        stmt.execute();
        this.disconnect();
    }

    public void delete(long id) throws SQLException {
        PreparedStatement stmt = null;
        this.connect();
        stmt = connection.prepareStatement(
                "DELETE FROM posts"
                + " WHERE id = ?"
        );
        stmt.setLong(1, id);
        stmt.execute();
        this.disconnect();
    }

    public Post getPost(long id) throws SQLException {
        LOG.info("get(id)");
        Post post = new Post();
        this.connect();
        PreparedStatement stmt = connection.prepareStatement(
                "SELECT * FROM posts"
                + " WHERE id = ?"
        );
        stmt.setLong(1, id);
        ResultSet rs = stmt.executeQuery();
        LOG.info("consulta hecha post");
        if (rs.next()) {
            LOG.info("Datos ...");
            post.setId(rs.getLong("id"));
            post.setAuthor(rs.getString("author"));
            post.setTitle(rs.getString("title"));
            post.setContent(rs.getString("content"));
            LOG.info("Datos cargados");
        } else {
            LOG.log(Level.INFO, "No hay datos para el id {0}", id);
        }
        
        
        this.disconnect();
        return post;
    }

    public void update(Post post) throws SQLException {
        this.connect();
        PreparedStatement stmt = connection.prepareStatement(
                "UPDATE posts SET "
                        + "author = ?, "
                        + "title = ?, "
                        + "content = ?, "
                       
                        + " WHERE id = ? "
        );
        stmt.setString(1, post.getAuthor());
        stmt.setString(2, post.getTitle());
        stmt.setString(3, post.getContent());
       
        stmt.setLong(4, post.getId());
        LOG.info("id : " + post.getId());
        stmt.execute();
        this.disconnect();
        return;
    }
}
